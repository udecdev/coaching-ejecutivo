window.onload = function() {
    // Código a ejecutar cuando se ha cargado toda la página
  document.body.style.visibility = "visible";
   // stage2.stop();
    main();
};
var audios = [
  {
    url: "./sounds/click.mp3",
    name: "clic"
  },
  {
    url: "./sounds/Coaching_Ejecutivo.mp4",
    name: "a1"
  },
  {
    url: "./sounds/Mentoring.mp4",
    name: "a2"
  }  
];
ivo.info({
    title: "Universidad de Cindinamarca",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var stage1 = new TimelineMax();
var stage2 = new TimelineMax();
var stage3 = new TimelineMax();
var stage4 = new TimelineMax();
function main() {
    var t = null;
    var udec = ivo.structure({
        created: function () {

            t = this;
            
            t.animations();
            t.events();
            //precarga audios//
            var onComplete = function () {
                ivo("#preload").hide();
                stage1.play();
            };
            ivo.load_audio(audios,onComplete );
        },
        methods: {
            events: function () {
                ivo("#btn_home").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage1.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage1__btn").on("click", function () {
                    stage1.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#btn_credits").on("click", function () {
                    stage3.timeScale(1).play();
                    ivo.play("clic");
                });
                

                ivo("#credits_close").on("click", function () {
                    stage3.timeScale(3).reverse();
                    ivo.play("clic");
                });
                ivo(".info").on("click", function () {
                  //traemos el data-modal
                  let modal = ivo(this).attr("data-modal");
                  ivo("#image1,#image2,#image3,#image4,#image5").hide();
                  ivo("#"+modal).show();
                  console.log(modal);
                  stage4.timeScale(1).play();
                  ivo.play("clic");
                });
                ivo(".close").on("click", function () {
                  ivo.play("clic");
                  stage4.timeScale(6).reverse();
                });
                ivo("#s1").on("click", function () {
                  ivo.play("a1");
                });
                ivo("#s2").on("click", function () {
                  ivo.play("a2");
                });
                let rules = function(rule){
                    console.log("rules"+rule);
                    
                };
                let onMove=function(page){
                    console.log("onMove"+page);
                    
                    ivo.play("clic");
                };
                let onFinish=function(){
                }
            
                var slider=ivo("#slider").slider({
                    slides:'.slider',
                    btn_next:"#btn_next",
                    btn_back:"#btn_back",
                    rules:rules,
                    onMove:onMove,
                    onFinish:onFinish
                });
            },
            animations: function () {

                stage1.append(TweenMax.from("#stage1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom("#stage1__title", .8, {y: 1300, opacity: 0}, .2), 0);
                stage1.append(TweenMax.from("#stage1__btn", .8, {x: 1300, opacity: 0,rotation:900}), 0);
                stage1.stop();
              
                stage2.append(TweenMax.from("#stage2", .8, {y: 7300, opacity: 0}), 0);
                stage2.stop();
              
                stage3.append(TweenMax.from("#modal-credits", .8, {y: 7300, opacity: 0}), 0);
                stage3.append(TweenMax.from("#credits_close", .8, {x: 300, opacity: 0}), 0);
                stage3.stop();

                stage4.append(TweenMax.from(".modal", .8, {y: 7300, opacity: 0}), 0);
                stage4.append(TweenMax.from(".close", .8, {x: 300, opacity: 0}), 0);
                stage4.stop();
                let movil = false;
                let windowWidth = window.innerWidth;
                if (windowWidth < 1024) {
                    movil = true;
                    
                }
                if('ontouchstart' in window || navigator.maxTouchPoints) {
                    movil = true;
                }
                if (movil) {
                    stage1.play();
                    stage2.play();
                    stage3.play();
                }

            }
        }
    });
}